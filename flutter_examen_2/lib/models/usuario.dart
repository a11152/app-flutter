import 'package:flutter_examen_2/util/global.dart';

class Usuario {

  String? name;
  String? email;
  String? pass;
  String? token;
  String? id;

  Usuario({
    this.name,
    this.email,
    this.pass,
    this.token,
    this.id,
  });

  Map<String, dynamic> toJson() => {
    'name': name,
    'email': email,
    'pass': pass,
    'token': token,
    'id': id,
  };

  Usuario.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    pass = json['pass'];
    token = json['token'];
    id = json['id'];
  }

  Usuario.fromJsonLogin(Map<String, dynamic> json) {
    name = json["user"]["name"];
    email = json["user"]["email"];
    pass = json["user"]["pass"];
    id = json["user"]["id"];
    token = json["token"];
  }

}