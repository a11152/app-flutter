class Tarea {

  String? titulo;
  String? descripcion;
  String? fecha;
  bool? estado;
  String? usuario;
  String? id;

  Tarea({
    this.descripcion,
    this.fecha,
    this.estado,
    this.usuario,
    this.id,
    this.titulo,
  });

  Map<dynamic, dynamic> toJson() => {
    'descripcion': descripcion,
    'fecha': fecha,
    'estado': estado,
    'usuario': usuario,
    'id': id,
    'titulo': titulo,
  };

  Tarea.fromJson(Map<dynamic, dynamic> json) {
    print(json);
    descripcion = json['descripcion'];
    fecha = json['fecha'];
    estado = json['estado'];
    usuario = json['usuario'];
    id = json['_id'];
    titulo = json['titulo'];
  }

}