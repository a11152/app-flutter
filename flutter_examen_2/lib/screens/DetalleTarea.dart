import 'package:flutter/material.dart';
import 'package:flutter_examen_2/models/tarea.dart';
import 'package:flutter_examen_2/services/tarea.dart';
import 'package:flutter_examen_2/util/global.dart';
import 'package:flutter_examen_2/widgets/buton_widget.dart';
import 'package:flutter_examen_2/widgets/card_widget.dart';
import 'package:flutter_examen_2/widgets/textfield_widget.dart';

class DetalleTarea extends StatefulWidget {

  final String? id;

  DetalleTarea({
    Key? key,
    this.id,
  }) : super(key: key);

  @override
  State<DetalleTarea> createState() => _DetalleTareaState(id: id);
}

class _DetalleTareaState extends State<DetalleTarea> {
  
  final String? id;
  final Tarea tarea = Tarea();

  final _controllerTitulo = TextEditingController();
  final _controllerDescripcion = TextEditingController();
  final _controllerFecha = TextEditingController();
  
  var estado = false;

  _DetalleTareaState({ this.id });

  @override
  void initState() {
    super.initState();
    print('id: $id');
    TareaService.getTarea(id!).then((value) {
      setState(() {
        tarea.id = value!.id;
        tarea.titulo = value.titulo;
        tarea.descripcion = value.descripcion;
        tarea.estado = value.estado;
        tarea.fecha = value.fecha;

        _controllerTitulo.text = tarea.titulo!;
        _controllerDescripcion.text = tarea.descripcion!;
        _controllerFecha.text = tarea.fecha!;
        print(tarea.estado);
        estado = tarea.estado!;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: SafeArea(
        child: Center(
          child: CardWidget(
            height: 500,
            child: tarea.estado != null ? Column(
              children: [
                Text(
                  'Detalle Tarea',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                SizedBox(height: 20),
                TextFieldWidget(
                  hintText: "Titulo",
                  isPrefixIcon: true,
                  isMyControllerActivated: true,
                  controller: _controllerTitulo,
                  prefixiconData: Icons.date_range,
                ),
                SizedBox(height: 20),
                TextFieldWidget(
                  hintText: "Descripcion",
                  isPrefixIcon: true,
                  isMyControllerActivated: true,
                  controller: _controllerDescripcion,
                  prefixiconData: Icons.description,
                ),
                SizedBox(height: 20),
                TextFieldWidget(
                  hintText: "Fecha",
                  isPrefixIcon: true,
                  isMyControllerActivated: true,
                  controller: _controllerFecha,
                  prefixiconData: Icons.date_range,
                ),
                SizedBox(height: 20),
                Checkbox(value: estado, onChanged: (value) {
                  setState(() {
                    estado = value ?? false;
                  });
                }),
                ButtonWidget(
                  hasBorder: false,
                  width: 200,
                  colorButton: Colors.greenAccent,
                  color: Colors.greenAccent,
                  title: "Actualizar",
                  onPressed: () {
                    if (
                      _controllerTitulo.text.isNotEmpty &&
                      _controllerDescripcion.text.isNotEmpty &&
                      _controllerFecha.text.isNotEmpty
                    ) {
                      tarea.titulo = _controllerTitulo.text;
                      tarea.descripcion = _controllerDescripcion.text;
                      tarea.fecha = _controllerFecha.text;
                      tarea.estado = estado;
                      TareaService.updateTarea(tarea).then((value) => {
                        if (value != null) {
                          Global.mensaje(context, "Se actualizo correctamente la tarea", "Actualizado", backgroundColorCustom: Colors.greenAccent)
                        } else {
                          Global.mensaje(context, "No se pudo actualizar la tarea", "Error")
                        }
                      });
                    } else {
                      Global.mensaje(context, "No dejes campos vacios", "Error");
                    }
                  },
                ),
              ]
            ) : const Center(
              child: CircularProgressIndicator(),
            ),
          )
        ),
      ),
    );
  }
}
