import 'package:flutter/material.dart';
import 'package:flutter_examen_2/models/tarea.dart';
import 'package:flutter_examen_2/screens/AgregarTareaScreen.dart';
import 'package:flutter_examen_2/screens/DetalleTarea.dart';
import 'package:flutter_examen_2/services/tarea.dart';
import 'package:flutter_examen_2/widgets/card_task.dart';

class TareasScreen extends StatefulWidget {
  const TareasScreen({Key? key}) : super(key: key);

  @override
  State<TareasScreen> createState() => _TareasScreenState();
}

class _TareasScreenState extends State<TareasScreen> with WidgetsBindingObserver {

  var v = false;

  @override
  void initState() {
    super.initState();
    print("initState");
    setState(() {
      v = !v;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Center(
              child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    ' Tareas',
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  IconButton(
                    icon: Icon(
                      Icons.add,
                      color: Colors.greenAccent,
                      size: 30,
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => AgregarTareaScreen()));
                    },
                  ),
                ],
              ),
              SizedBox(height: 20),
              v ? FutureBuilder(
                future: TareaService.getTareas(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data.length == 0) {
                      return const Center(
                        child: Text(
                          'No hay tareas',
                          style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      );
                    }
                    return ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        print("Hola");
                        print(snapshot.data[index].estado);
                        if (!snapshot.data[index].estado) {
                          return CardTask(
                            title: snapshot.data[index].titulo,
                            description: snapshot.data[index].descripcion,
                            date: snapshot.data[index].fecha,
                            color: Colors.red,
                            id: snapshot.data[index].id,
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => DetalleTarea(
                                        id: snapshot.data[index].id,
                                      ))).then((value) => {
                                        setState(() {
                                          v = !v;
                                        })
                                      });
                            },
                          );
                        } else {
                          return CardTask(
                            title: snapshot.data[index].titulo,
                            description: snapshot.data[index].descripcion,
                            date: snapshot.data[index].fecha,
                            color: Colors.green,
                            id: snapshot.data[index].id,
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => DetalleTarea(
                                        id: snapshot.data[index].id,
                                      ))).then((value) => {
                                        setState(() {
                                          v = true;
                                        })
                                      });
                            },
                          );
                        }
                      },
                    );
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }
              ) : Center(
                child: CircularProgressIndicator(),
              ),
              SizedBox(height: 20),
            ],
          )),
        ),
      ),
    );
  }
}
