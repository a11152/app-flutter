import 'package:flutter/material.dart';
import 'package:flutter_examen_2/models/tarea.dart';
import 'package:flutter_examen_2/services/tarea.dart';
import 'package:flutter_examen_2/util/global.dart';
import 'package:flutter_examen_2/widgets/buton_widget.dart';
import 'package:flutter_examen_2/widgets/card_widget.dart';
import 'package:flutter_examen_2/widgets/textfield_widget.dart';

class AgregarTareaScreen extends StatefulWidget {
  const AgregarTareaScreen({Key? key}) : super(key: key);

  @override
  State<AgregarTareaScreen> createState() => _AgregarTareaScreenState();
}

class _AgregarTareaScreenState extends State<AgregarTareaScreen> {

  final _controllerTitulo = TextEditingController();
  final _controllerDescripcion = TextEditingController();
  final _controllerFecha = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      body: SafeArea(
        child: Center(
          child: CardWidget(
            height: 500,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Agregar Tarea',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                SizedBox(height: 40),
                TextFieldWidget(
                  hintText: "Titulo",
                  isPrefixIcon: true,
                  isMyControllerActivated: true,
                  controller: _controllerTitulo,
                  prefixiconData: Icons.title,
                ),
                SizedBox(height: 20),
                TextFieldWidget(
                  hintText: "Descripcion",
                  isPrefixIcon: true,
                  isMyControllerActivated: true,
                  controller: _controllerDescripcion,
                  prefixiconData: Icons.description,
                ),
                SizedBox(height: 20),
                TextFieldWidget(
                  hintText: "Fecha",
                  isPrefixIcon: true,
                  isMyControllerActivated: true,
                  controller: _controllerFecha,
                  prefixiconData: Icons.date_range,
                ),
                SizedBox(height: 20),
                ButtonWidget(
                  hasBorder: false,
                  width: 200,
                  colorButton: Colors.greenAccent,
                  color: Colors.greenAccent,
                  title: "Agregar",
                  onPressed: () {

                    if ( _controllerTitulo.text.isEmpty ||
                        _controllerDescripcion.text.isEmpty ||
                        _controllerFecha.text.isEmpty) {
                      Global.mensaje(context, "Todos los campos deben estar llenos", "Error");
                    } else {
                      TareaService.createTarea(Tarea(
                        titulo: _controllerTitulo.text,
                        descripcion: _controllerDescripcion.text,
                        fecha: _controllerFecha.text,
                        estado: false,
                      )).then((value) => {
                        if (value != null) {
                          Global.mensaje(context, "Tarea agregada correctamente", "Exito", backgroundColorCustom: Colors.greenAccent),
                          _controllerDescripcion.clear(),
                          _controllerFecha.clear(),
                          _controllerTitulo.clear(),
                        } else {
                          Global.mensaje(context, "Error al agregar la tarea", "Error")
                        }
                      });
                    }

                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
