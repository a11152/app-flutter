import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_examen_2/screens/TareasScreen.dart';
import 'package:flutter_examen_2/services/usuario.dart';
import 'package:flutter_examen_2/util/global.dart';
import 'package:flutter_examen_2/widgets/buton_widget.dart';
import 'package:flutter_examen_2/widgets/card_widget.dart';
import 'package:flutter_examen_2/widgets/textfield_widget.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var isPassSee = false;

  var _correoController = TextEditingController();
  var _passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.blueAccent,
        body: SafeArea(
          child: Center(
              child: FadeInUp(
            delay: const Duration(milliseconds: 1500),
            child: CardWidget(
              height: 500,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Login',
                    style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 20),
                  TextFieldWidget(
                    hintText: 'Correo',
                    prefixiconData: Icons.email,
                    isMyControllerActivated: true,
                    controller: _correoController,
                    isPrefixIcon: true,
                  ),
                  SizedBox(height: 20),
                  TextFieldWidget(
                    hintText: 'Contraseña',
                    isMyControllerActivated: true,
                    controller: _passController,
                    prefixiconData: Icons.lock,
                    isPrefixIcon: true,
                    obscureText: !isPassSee,
                    isSuffixIcon: true,
                    suffixiconData:
                        isPassSee ? Icons.visibility : Icons.visibility_off,
                    onSuffixIconTab: () {
                      setState(() {
                        isPassSee = !isPassSee;
                      });
                    },
                  ),
                  SizedBox(height: 20),
                  ButtonWidget(
                    hasBorder: false,
                    width: 250,
                    title: 'Ingresar',
                    onPressed: () {
                      if (
                        !_correoController.text.isNotEmpty ||
                        !_passController.text.isNotEmpty
                      ) {
                        Global.mensaje(context, "No dejar campos vacios", "Error");
                      } else {
                        UsuarioServices.login(_correoController.text, _passController.text).then((value) {
                          if (value != null) {
                            Global.mensaje(context, "Bienvenido", value.name!, backgroundColorCustom: Colors.greenAccent);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => TareasScreen()));
                          } else {
                            Global.mensaje(context, "Usuario o contraseña incorrectos", "Error");
                          }
                        });
                      }
                    },
                  )
                ],
              ),
            ),
          )),
        ));
  }
}
