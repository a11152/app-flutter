import 'package:flutter/material.dart';
import 'package:flutter_examen_2/shared/responsive.dart';
import 'package:flutter_examen_2/util/global.dart';

class TextFieldWidget extends StatelessWidget {

  final String? hintText;
  final IconData? prefixiconData;
  final IconData? suffixiconData;
  final void Function(String)? onChanged;
  final bool? obscureText;
  final bool? isSuffixIcon;
  final bool? isPrefixIcon;
  final void Function()? onTap;
  final bool? readOnly;
  final bool? otherColor;
  final Color? color;
  final int? maxLong;
  final void Function()? onSuffixIconTab;
  final bool? isMaxLong;
  final bool? isFilled;
  final TextEditingController? controller;
  final bool? isMyControllerActivated;

  const TextFieldWidget({
    Key? key,
    this.hintText,
    this.prefixiconData,
    this.suffixiconData,
    this.onChanged,
    this.obscureText = false,
    this.isSuffixIcon = false,
    this.isPrefixIcon = false,
    this.onTap,
    this.readOnly = false,
    this.otherColor = false,
    this.color = Global.ColorEmpresa,
    this.maxLong = 50,
    this.onSuffixIconTab,
    this.isMaxLong = false,
    this.isFilled = false,
    this.controller,
    this.isMyControllerActivated = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double fontSizeButton = 0;
    if (isTab(context) || isDesktop(context)) {
      fontSizeButton = 20;
    } else if (isMobile(context)) {
      fontSizeButton = 16;
    }

    return TextFormField(
      maxLength: isMaxLong! ? maxLong : null,
      readOnly: readOnly!,
      controller: isMyControllerActivated! ? controller : null,
      onChanged: onChanged,
      obscureText: obscureText!,
      textInputAction: TextInputAction.done,
      onTap: onTap,
      cursorColor: color,
      style: TextStyle(fontSize: fontSizeButton, color: color),
      decoration: InputDecoration(
        labelStyle: TextStyle(
          color: color,
        ),
        focusColor: color,
        filled: isFilled,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: BorderSide(
            width: 2.0,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
          borderSide: BorderSide(
            width: 2.0,
          ),
        ),
        labelText: hintText,
        prefixIcon: isPrefixIcon! ? Icon(
          prefixiconData,
          color: color,
          size: 30,
        ) : null,
        suffixIcon: isSuffixIcon! ? GestureDetector(
          onTap: onSuffixIconTab,
          child: Icon(
            suffixiconData,
            color: color,
            size: 25,

          ),
        ) : null,

      ),
    );
  }
}
