import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {

  final Widget? child;
  final double? height;
  
  // ignore: use_key_in_widget_constructors
  CardWidget({@required this.child, @required this.height}) : super();

  @override
  Widget build(BuildContext context) {
    return Card(
      child: SizedBox(
        height: height ?? MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width * 0.9,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Center(
            child: child
          ),
        ),
      ),
    );
  }
}
