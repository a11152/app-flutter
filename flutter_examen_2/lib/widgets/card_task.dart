import 'dart:ffi';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_examen_2/screens/DetalleTarea.dart';

class CardTask extends StatelessWidget {
  final Color? color;
  final String? title;
  final String? description;
  final bool state;
  final String? date;
  final String? id;
  final void Function()? onTap;

  CardTask({
    this.color,
    this.title,
    this.description,
    this.state = false,
    this.date,
    this.id,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return FadeInUp(
      duration: Duration(milliseconds: 500),
      child: Card(
        color: color ?? Colors.greenAccent,
        child: InkWell(
          onTap: onTap,
          child: Column(
            children: [
              ListTile(
                title: Text(
                  title ?? 'Tarea',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
                subtitle: Text(
                  description ?? 'Descripcion',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
                trailing: Icon(
                  state ? Icons.check : Icons.error,
                  color: Colors.white,
                ),
              ),
              ListTile(
                title: Text(
                  date!.isNotEmpty ? DateTime.parse(date!).year.toString() + "/" + DateTime.parse(date!).month.toString() + "/" + DateTime.parse(date!).day.toString() : 'Fecha',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
