import 'package:flutter_examen_2/shared/responsive.dart';
import 'package:flutter_examen_2/util/global.dart';
import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {

  final String? title;
  final bool? hasBorder;
  final void Function()? onPressed;
  final Color? color;
  final double? height;
  final double? width;
  final double? fontSize;
  final bool? otherColor;
  final Color? colorButton;

  const ButtonWidget({
    Key? key,
    this.title,
    this.hasBorder,
    this.onPressed,
    this.color,
    this.height = 60,
    this.width = 100,
    this.fontSize,
    this.otherColor = false,
    this.colorButton = Global.ColorEmpresa,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double fontSizeButton = 0;
    if (isTab(context) || isDesktop(context)) {
      fontSizeButton = fontSize ?? 20;
    } else if (isMobile(context)) {
      fontSizeButton = fontSize ?? 16;
    }

    return Material(
      borderRadius: BorderRadius.circular(4.0),
      child: Ink(
          decoration: BoxDecoration(
            color: hasBorder!
                ? Global.ColorBlanco
                : otherColor!
                    ? colorButton
                    : Global.ColorEmpresa,
            borderRadius: BorderRadius.circular(4.0),
          ),
          child: InkWell(
            borderRadius: BorderRadius.circular(4.0),
            onTap: onPressed,
            child: SizedBox(
              height: height,
              width: width,
              child: Center(
                child: Text(
                  title ?? '',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: fontSizeButton,
                    color: hasBorder!
                        ? Global.ColorBlanco
                        : otherColor!
                            ? colorButton
                            : Global.ColorBlanco,
                  ),
                ),
              ),
            ),
          )),
    );
  }
}
