import 'dart:convert';

import 'package:flutter_examen_2/models/usuario.dart';
import 'package:flutter_examen_2/services/sharedPrefrence.dart';
import 'package:http/http.dart' as http;

class UsuarioServices {
  static getUsuarioToShared() async {
    final usuarioJson = await SharedPreferenceService.read('usuario');
    return usuarioJson == null ? null : Usuario.fromJson(usuarioJson);
  }

  static saveUsuarioToShared(Usuario usuario) async {
    await SharedPreferenceService.save('usuario', usuario.toJson());
  }

  static Future<String> getID() async {
    final usuario = await SharedPreferenceService.read('usuario');
    return usuario["id"];
  }

  static Future<Usuario?> login(String correo, String pass) async {
    final uri = Uri.parse("http://10.0.2.2:3001/api/v1/usuarios/login");
    try {
      final response =
          await http.post(uri, body: {'email': correo, 'pass': pass});

      if (response.statusCode == 200) {
        final usuario = Usuario.fromJsonLogin(json.decode(response.body));
        print("usuario logged");
        UsuarioServices.saveUsuarioToShared(usuario);
        return usuario;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
