
import 'package:flutter_examen_2/models/tarea.dart';
import 'package:flutter_examen_2/models/usuario.dart';
import 'package:flutter_examen_2/services/sharedPrefrence.dart';
import 'package:flutter_examen_2/services/usuario.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class TareaService {

  static Future<Tarea?> updateTarea(Tarea tarea) async {
    final uri = Uri.parse("http://10.0.2.2:3001/api/v1/tareas/${tarea.id}");
    print(tarea.estado.runtimeType);
    final response = await http.put(uri, body: {
      'titulo': tarea.titulo,
      'descripcion': tarea.descripcion,
      'estado': tarea.estado.toString(),
      'fecha': tarea.fecha,
    });

    if (response.statusCode == 200) {
      final body = json.decode(response.body) as Map;
      print(body["data"]);
      return Tarea();
    } else {
      return null;
    }

  }


  static Future<Tarea?> getTarea(String id) async {
    final uri = Uri.parse("http://10.0.2.2:3001/api/v1/tareas/$id");
    try {
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        final body = json.decode(response.body) as Map;
        final tarea = Tarea.fromJson(body["data"]);
        return tarea;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  static Future<Tarea?> createTarea(Tarea tarea) async {
    final id = await UsuarioServices.getID();

    print(id);

    final uri = Uri.parse("http://10.0.2.2:3001/api/v1/tareas");
    final tareaJson = await http.post(uri, body: {
      'usuario': id,
      'titulo': tarea.titulo,
      'descripcion': tarea.descripcion,
      'fecha': tarea.fecha,
      'estado': tarea.estado,
    });

    if (tareaJson.statusCode == 200) {
      final body = json.decode(tareaJson.body) as Map;
      print(body);
      final tarea = Tarea.fromJson(body["data"]);
      return tarea;
    } else {
      return null;
    }

  }

  static Future<List<Tarea>?> getTareas() async {
    final usuario = await SharedPreferenceService.read('usuario');
    final id = usuario["id"];

    final uri = Uri.parse("http://10.0.2.2:3001/api/v1/tareas/user/$id");
    final tareasJson = await http.get(uri);

    if (tareasJson.statusCode == 200) {
      final response = json.decode(tareasJson.body) as Map;
      print(response["data"]);
      final tareas = (response["data"] as List)
          .map((tarea) => Tarea.fromJson(tarea))
          .toList();
      print(tareas);
      return tareas;
    } else {
      return [];
    }

  }

}
