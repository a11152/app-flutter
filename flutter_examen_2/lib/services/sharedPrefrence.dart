import 'dart:convert';
import 'package:flutter_examen_2/util/global.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceService {

  static final Future<SharedPreferences> _prefs = Global.pref;

  static save(String key, value) async {
    (await _prefs).setString(key, json.encode(value));
  }

  static read(String key) async {
    return json.decode(
      (await _prefs).getString(key) ?? '',
    );
  }

}