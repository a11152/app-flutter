import 'package:flutter/material.dart';
import 'package:flutter_examen_2/models/usuario.dart';
import 'package:flutter_examen_2/screens/LoginScreen.dart';
import 'package:flutter_examen_2/screens/TareasScreen.dart';
import 'package:flutter_examen_2/services/usuario.dart';
import 'package:flutter_examen_2/util/global.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Usuario? usuario;

  @override
  void initState() {
    super.initState();
    setState(() {
      UsuarioServices.getUsuarioToShared().then((usuario) {
        setState(() {
          this.usuario = usuario;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter App Segundo Parcial',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: usuario?.email?.isNotEmpty == true
          ? TareasScreen()
          : LoginScreen()
    );
  }
}
